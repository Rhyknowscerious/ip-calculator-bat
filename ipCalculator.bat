:: This Source Code Form is subject to the terms of the Mozilla Public
:: License, v. 2.0. If a copy of the MPL was not distributed with this
:: file, You can obtain one at https://mozilla.org/MPL/2.0/. 

:: Copyright 2017 Ryan James Decker

@ECHO OFF 

SET _title=IP Address Calculator
TITLE %_title%

:_main
CLS
SET _IPAddress=
SET _netMask=
SET _invertedNetMask=
SET _totalHostCount=
SET _useableHostCount=
SET _networkAddress=
SET _broadcastAddress=
ECHO Welcome to IP Address Calculator & ECHO.

CALL :_getNetworkProfile
CALL :_showNetworkProfile
ECHO. & PAUSE & GOTO :_main

:: USAGE
REM :_checkIPFormat IPAddress
:_checkIPFormat
	IF [%~1]==[] (
		ECHO. & ECHO You didn't enter an IP address. & ECHO.
		PAUSE
		GOTO :_main
	)
	ECHO %~1 | FINDSTR /R "[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*" > NUL && (
		SETLOCAL ENABLEDELAYEDEXPANSION
			SET _IPAddressIsValid=TRUE
			FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%a IN ('%~1') DO (
				IF %%a LSS 0 SET _IPAddressIsValid=FALSE
				IF %%a GTR 255 SET _IPAddressIsValid=FALSE
				IF %%b LSS 0 SET _IPAddressIsValid=FALSE
				IF %%b GTR 255 SET _IPAddressIsValid=FALSE
				IF %%c LSS 0 SET _IPAddressIsValid=FALSE
				IF %%c GTR 255 SET _IPAddressIsValid=FALSE
				IF %%d LSS 0 SET _IPAddressIsValid=FALSE
				IF %%d GTR 255 SET _IPAddressIsValid=FALSE
			)
			IF !_IPAddressIsValid!==TRUE (
				ECHO IP Address %~1 is VALID > NUL
			) ELSE (
				ECHO. & ECHO Bad octet^(s^) in IP Address: [%~1] & ECHO.
				PAUSE
				GOTO :_main
			)
		ENDLOCAL
	) || (
		ECHO. & ECHO [%~1]: Bad IP Address Format & ECHO.
		PAUSE
		GOTO :_main
	)
EXIT /B 0

:: USAGE
REM :_checkNetMaskFormat NetworkMask
:_checkNetMaskFormat
	IF [%~1]==[] (
		ECHO. & ECHO You didn't enter a network mask. & ECHO.
		PAUSE
		GOTO :_main
	)
	ECHO %~1 | FINDSTR /R "[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*" > NUL && (
		SETLOCAL ENABLEDELAYEDEXPANSION
			SET _maskOctet1Valid=FALSE
			SET _maskOctet2Valid=FALSE
			SET _maskOctet3Valid=FALSE
			SET _maskOctet4Valid=FALSE
			FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%a IN ('%~1') DO (
				SET /A _maskOctet1=%%a
				SET /A _maskOctet2=%%b
				SET /A _maskOctet3=%%c
				SET /A _maskOctet4=%%d
				
				IF %%a==0 SET _maskOctet1Valid=FALSE
				IF %%a==128 SET _maskOctet1Valid=TRUE
				IF %%a==192 SET _maskOctet1Valid=TRUE
				IF %%a==224 SET _maskOctet1Valid=TRUE
				IF %%a==240 SET _maskOctet1Valid=TRUE
				IF %%a==248 SET _maskOctet1Valid=TRUE
				IF %%a==252 SET _maskOctet1Valid=TRUE
				IF %%a==254 SET _maskOctet1Valid=TRUE
				IF %%a==255 SET _maskOctet1Valid=TRUE
				
				IF %%b==0 SET _maskOctet2Valid=TRUE
				IF %%b==128 SET _maskOctet2Valid=TRUE
				IF %%b==192 SET _maskOctet2Valid=TRUE
				IF %%b==224 SET _maskOctet2Valid=TRUE
				IF %%b==240 SET _maskOctet2Valid=TRUE
				IF %%b==248 SET _maskOctet2Valid=TRUE
				IF %%b==252 SET _maskOctet2Valid=TRUE
				IF %%b==254 SET _maskOctet2Valid=TRUE
				IF %%b==255 SET _maskOctet2Valid=TRUE
				
				IF %%c==0 SET _maskOctet3Valid=TRUE
				IF %%c==128 SET _maskOctet3Valid=TRUE
				IF %%c==192 SET _maskOctet3Valid=TRUE
				IF %%c==224 SET _maskOctet3Valid=TRUE
				IF %%c==240 SET _maskOctet3Valid=TRUE
				IF %%c==248 SET _maskOctet3Valid=TRUE
				IF %%c==252 SET _maskOctet3Valid=TRUE
				IF %%c==254 SET _maskOctet3Valid=TRUE
				IF %%c==255 SET _maskOctet3Valid=TRUE
				
				IF %%d==0 SET _maskOctet4Valid=TRUE
				IF %%d==128 SET _maskOctet4Valid=TRUE
				IF %%d==192 SET _maskOctet4Valid=TRUE
				IF %%d==224 SET _maskOctet4Valid=TRUE
				IF %%d==240 SET _maskOctet4Valid=TRUE
				IF %%d==248 SET _maskOctet4Valid=TRUE
				IF %%d==252 SET _maskOctet4Valid=TRUE
				IF %%d==254 SET _maskOctet4Valid=TRUE
				IF %%d==255 SET _maskOctet4Valid=TRUE
				
			)
			IF !_maskOctet4! GTR 0 (
				IF !_maskOctet3! NEQ 255 (
					SET !_maskOctet3Valid=FALSE
			))
			IF !_maskOctet3! GTR 0 (
				IF !_maskOctet2! NEQ 255 (
					SET _maskOctet3Valid=FALSE
			))
			IF !_maskOctet2! GTR 0 (
				IF !_maskOctet1! NEQ 255 (
					SET _maskOctet3Valid=FALSE
			))
			
			SET _netMaskIsValid=TRUE
			IF !_maskOctet1Valid!==FALSE SET _netMaskIsValid=FALSE
			IF !_maskOctet2Valid!==FALSE SET _netMaskIsValid=FALSE
			IF !_maskOctet3Valid!==FALSE SET _netMaskIsValid=FALSE
			IF !_maskOctet4Valid!==FALSE SET _netMaskIsValid=FALSE
			
			IF !_netMaskIsValid!==TRUE (
				ECHO Network Mask %~1 is VALID > NUL
			) ELSE (
				ECHO. & ECHO Bad octet^(s^) in Network Mask [%~1] & ECHO.
				PAUSE
				GOTO :_main
			)
		ENDLOCAL
	) || (
		ECHO. & ECHO [%~1]: Bad Network Mask Format & ECHO.
		PAUSE
		GOTO :_main
	)
EXIT /B 0

:: USAGE
REM :_getBroadcastAddress NetworkAddress NetworkMask
:_getBroadcastAddress
	SETLOCAL ENABLEDELAYEDEXPANSION
		FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%a IN ('%~2') DO (
			FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%A IN ('%~1') DO (
				SET /A "_invertOctet1=255^%%a"
				SET /A "_invertOctet2=255^%%b"
				SET /A "_invertOctet3=255^%%c"
				SET /A "_invertOctet4=255^%%d"
				SET _invertedNetMask=!_invertOctet1!.!_invertOctet2!.!_invertOctet3!.!_invertOctet4!
				
				SET /A "_broadcastOctet1=!_invertOctet1!|%%A"
				SET /A "_broadcastOctet2=!_invertOctet2!|%%B"
				SET /A "_broadcastOctet3=!_invertOctet3!|%%C"
				SET /A "_broadcastOctet4=!_invertOctet4!|%%D"
				SET "_broadcastAddress=!_broadcastOctet1!.!_broadcastOctet2!.!_broadcastOctet3!.!_broadcastOctet4!
			)
		)
	ENDLOCAL & (
		SET _invertedNetMask=%_invertedNetMask%
		SET _broadcastAddress=%_broadcastAddress%
	)
EXIT /B 0

:: USAGE
REM :_getHostCount NetworkMask
:_getHostCount
	FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%a IN ('%~1') DO (
		SET /A _totalHostCount="((255^%%a)+1)*((255^%%b)+1)*((255^%%c)+1)*((255^%%d)+1)"
	)
	IF %_totalHostCount% EQU 2 (
		SET /A _useableHostCount=%_totalHostCount%
	) ELSE IF %_totalHostCount% EQU 1 (
		SET /A _useableHostCount=%_totalHostCount%
	) ELSE (
		SET /A _useableHostCount=%_totalHostCount%-2
	)
EXIT /B 0

:: USAGE
REM :_getNetworkAddress IPAddress NetworkMask
:_getNetworkAddress
	SETLOCAL ENABLEDELAYEDEXPANSION
		FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%a IN ('%~1') DO (
			FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%A IN ('%~2') DO (SET /A "_networkOctet1=%%a&%%A"
				SET /A "_networkOctet2=%%b&%%B"
				SET /A "_networkOctet3=%%c&%%C"
				SET /A "_networkOctet4=%%d&%%D"
				SET _networkAddress=!_networkOctet1!.!_networkOctet2!.!_networkOctet3!.!_networkOctet4!
			)
		)
	ENDLOCAL & (
		SET _networkAddress=%_networkAddress%
	)
EXIT /B 0

:: USAGE
REM :_getNetworkProfile
:_getNetworkProfile
	SET /P _IPAddress=Enter an IPv4 network, host, or broadcast address in dot decimal format: 
	CALL :_checkIPFormat %_IPAddress%
	SET /P _netMask=Enter a contiguous network mask in dot decimal format: 
	CALL :_checkNetMaskFormat %_netMask%
	
	CALL :_getHostCount %_netMask%
	CALL :_getNetworkAddress %_IPAddress% %_netMask%
	CALL :_getBroadcastAddress %_networkAddress% %_netMask% & ECHO.
	CALL :_getRange %_IPAddress% %_netMask%
EXIT /B 0

:: USAGE
REM :_getRange IPAddress NetworkMask
:_getRange
	CALL :_getTargetOctetFromMask %~2
	CALL :_getNetworkAddress %~1 %~2
	CALL :_getBroadcastAddress %_networkAddress% %~2
	FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%a IN ('%_networkAddress%') DO (
		FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%A IN ('%_broadcastAddress%') DO (
			
			SET /A _firstHostOctet1=%%a
			SET /A _firstHostOctet2=%%b
			SET /A _firstHostOctet3=%%c
			SET /A "_firstHostOctet4=%%d+1"
			
			SET /A _lastHostOctet1=%%A
			SET /A _lastHostOctet2=%%B
			SET /A _lastHostOctet3=%%C
			SET /A "_lastHostOctet4=%%D-1"
			
			FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%W IN ('%_netMask%') DO (
				IF %_targetOctet% EQU 4 (
					IF %%Z EQU 254 (
						SET /A "_firstHostOctet4=%%d
						SET /A "_lastHostOctet4=%%D
					)
					IF %%Z EQU 255 (
						SET /A "_firstHostOctet4=%%d"
						SET /A "_lastHostOctet4=%%d"
					)
				)
			)
		)
	)
	SET _firstHostAddress=%_firstHostOctet1%.%_firstHostOctet2%.%_firstHostOctet3%.%_firstHostOctet4%
	SET _lastHostAddress=%_lastHostOctet1%.%_lastHostOctet2%.%_lastHostOctet3%.%_lastHostOctet4%
EXIT /B 0

:: USAGE
REM :_getTargetOctetFromMask NetworkMask
:_getTargetOctetFromMask
	FOR /F "USEBACKQ TOKENS=1-4 DELIMS=." %%a IN ('%~1') DO (
		IF %%c EQU 255 SET /A _targetOctet=4
		IF %%b EQU 255 (
			IF %%c NEQ 255 SET /A _targetOctet=3
		)
		IF %%a EQU 255 (
			IF %%b NEQ 255 SET /A _targetOctet=2
		)
		IF %%a NEQ 255 SET /A _targetOctet=1
	)
EXIT /B 0

:: USAGE
REM :_showNetworkProfile
:_showNetworkProfile
	IF %_totalHostCount% EQU 2 (
		ECHO Note:
		ECHO With network mask %_netMask%, the network and first host addresses are the same. Also, the last host and broadcast addresses are the same. So you'll only have two IP addresses in the %_networkAddress% network. Be aware that some older networking equipment may not support this network mask: & ECHO.
	)
	IF %_totalHostCount% EQU 1 (
		ECHO Note:
		ECHO With network mask %_netMask%, the network, host, and broadcast addresses share a single IP address. So you'll only have one IP address in network %_networkAddress%: & ECHO.
	)
		
	ECHO IP Address:         %_IPAddress%
	ECHO Network Mask:       %_netMask%
	ECHO Inverted Net Mask:  %_invertedNetMask% & ECHO.
	
	ECHO Total IP Addresses: %_totalHostCount%
	ECHO Host Addresses:     %_useableHostCount% & ECHO.
	
	IF %_firstHostAddress% EQU %_lastHostAddress% (
		SET /P "=Only IP Address:    " < NUL
		ECHO %_firstHostAddress%
	) ELSE (
		ECHO Network Address:    %_networkAddress%
		ECHO First Host Address: %_firstHostAddress%
		ECHO Last Host Address:  %_lastHostAddress%
		ECHO Broadcast Address:  %_broadcastAddress%
	)
	
	::ECHO What class?
	::ECHO How many network bits?
	::ECHO How many host bits?
	::ECHO How many networks?
	::ECHO How many hosts?
	::ECHO Public, private, APIPA, Loopback, last resort, special broadcast?
EXIT /B 0